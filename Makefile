# This Makefile was inspired by the Makefile of the package git2r
# (Version: 0.18.0.9000)

# Determine package name and version from DESCRIPTION file
PKG_VERSION=$(shell grep -i ^version DESCRIPTION | cut -d : -d \  -f 2)
PKG_NAME=$(shell grep -i ^package DESCRIPTION | cut -d : -d \  -f 2)

# Name of built package
PKG_TAR=$(PKG_NAME)_$(PKG_VERSION).tar.gz

#data-raw
DR=./data-raw

# Install package
install: data
	cd .. && R CMD INSTALL $(PKG_NAME)

# Download data
data:
	wget 'http://archaeologydataservice.ac.uk/archiveDS/archiveDownload?t=arch-1115-2/dissemination/csv/pottery/pottery.csv' -O $(DR)/pottery.csv
	wget 'http://archaeologydataservice.ac.uk/archiveDS/archiveDownload?t=arch-1115-2/dissemination/csv/pottery/counts.csv' -O $(DR)/counts.csv
	wget 'http://archaeologydataservice.ac.uk/catalogue/adsdata/arch-1115-2/dissemination/zip/tracts.zip' -O $(DR)/tracts.zip; unzip -o $(DR)/tracts.zip -d $(DR)
	wget 'https://gitlab.com/nehemie/copernicus-data/raw/4b50f2cfa4400ca7e6b8962f7371896709711825/data/antikythera_survey_project/asp_bg.tif' -O $(DR)/asp_bg.tif
	Rscript --vanilla  $(DR)/data-make.R

# Build documentation with roxygen
# 1) Remove old doc
# 2) Generate documentation
roxygen:
	rm -f man/*.Rd
	cd .. && Rscript -e "library(roxygen2); roxygenize('$(PKG_NAME)')"

# Generate PDF output from the Rd sources
# 1) Rebuild documentation with roxygen
# 2) Generate pdf, overwrites output file if it exists
pdf: roxygen
	cd .. && R CMD Rd2pdf --force $(PKG_NAME)

# Build and check package
check:
	cd .. && R CMD build $(PKG_NAME)
	cd .. && _R_CHECK_CRAN_INCOMING_=FALSE NOT_CRAN=true \
        R CMD check $(PKG_TAR)

check-no-vignette:
	cd .. && R CMD build $(PKG_NAME)
	cd .. && R CMD check --no-vignettes --no-build-vignettes $(PKG_TAR)

check-as-cran:
	cd .. && R CMD build $(PKG_NAME)
	cd .. && R CMD check --as-cran $(PKG_TAR)
