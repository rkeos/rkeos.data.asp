# The rkeos.data.asp package

Status: [![pipeline
status](https://gitlab.com/rkeos/rkeos.data.asp/badges/master/pipeline.svg)](https://gitlab.com/rkeos/rkeos.data.asp/commits/master)

This package contains some of datasets of the Antikythera Survey Project.

> Bevan, A and Conolly, J 2012 Intensive Survey Data from Antikythera,
> Greece. Journal of Open Archaeology Data 1:e3, DOI:
> http://dx.doi.org/10.5334/4f3bcb3f7f21d
>
>
> The Antikythera Survey Project was an interdisciplinary programme of
> fieldwork, artefact study and laboratory analysis that considered
> the long-term history and human ecology of the small Greek island of
> Antikythera. It was co-directed by Andrew Bevan (UCL) and James
> Conolly (Trent), in collaboration with Aris Tsaravopoulos (Greek
> Archaeological Service), and under the aegis of the Canadian
> Institute in Greece and the Hellenic Ministry of Culture. Its
> various primary datasets are unusual, both in the Mediterranean and
> beyond, for providing intensive survey coverage of an entire
> island’s surface archaeology.
>



The source of the package (https://gitlab.com/rkeos/rkeos.data.asp)
contains a folder `data-raw` and a script `data-make.R` explaining
which data-sets have been used and how it was packaged.

